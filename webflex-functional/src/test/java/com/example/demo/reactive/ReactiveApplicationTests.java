package com.example.demo.reactive;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ReactiveApplicationTests {

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void whenGetHelloRequestThenHelloWorldResponse() {
		String response = this.restTemplate.getForObject("/hello", String.class);
		Assert.assertEquals("Hello World", response);
	}

	@Test
	public void testHaliBeli() {
		final String response = this.restTemplate.getForObject("/hali", String.class);
		Assert.assertEquals("beli", response);
	}
	@Test
	public void contextLoads() {

	}

}
