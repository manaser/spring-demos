package com.example.demo.reactive;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.function.server.*;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import static org.springframework.web.reactive.function.server.RequestPredicates.*;

@Configuration
@EnableWebFlux
public class RoutingConfiguration {

//    HandlerFunction<ServerResponse> helloHandler = request ->
//        ServerResponse.ok().body(Mono.just("Hello World"), String.class);

    //HandlerFunction<ServerResponse> helloHandler;

    HandlerFunction<ServerResponse> handlerFunction;

    @Autowired
    public void setHandlerFunction(HandlerFunction<ServerResponse> handlerFunction) {
        this.handlerFunction = handlerFunction;
    }

//    @Bean
//    @Autowired
//    public RouterFunction<ServerResponse> helloWorldRouter(HandlerFunction<ServerResponse> handlerFunction) {
//        return RouterFunctions.route(RequestPredicates.GET("/hello"), handlerFunction::handle);
//    }

    @Bean
    public RouterFunction<ServerResponse> myRouter() {
        return RouterFunctions.route(GET("/hello"), handlerFunction::handle)
                .andRoute(GET("/ping"), serverRequest -> ServerResponse.ok().body(Mono.just("pong"), String.class))
                .andRoute(GET("/hali"), serverRequest -> ServerResponse.ok().body(Mono.just("beli"), String.class));
    }

}
